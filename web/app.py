from flask import Flask, render_template, request
import os

app = Flask(__name__)

@app.errorhandler(404)
def not_found(error):
    url = request.environ['REQUEST_URI']
    if "//" in url or "~" in url or ".." in url:
        return render_template('403.html'), 403
    else:
        return render_template('404.html'), 404


@app.route('/<path:subpath>')
def geturl(subpath):
    url = request.environ['REQUEST_URI']
    if "//" in url or "~" in url or ".." in url:
        return render_template('403.html'), 403
    urlpath = url.split('/')
    path_length = len(urlpath)
    root = './templates/'
    for x in range(path_length):
        if '.html' in urlpath[x]:
            if x == 1:
                source_path = os.path.join('./templates', urlpath[1])
            else:
                source_path = os.path.join(root + urlpath[x-1] + '/', urlpath[x])
        else:
            source_path = os.path.join('./templates', urlpath[1])
            if x != 0:
                root += urlpath[x-1] + '/'
            x += 1
    try:
        with open(source_path, 'r') as source:
            return render_template(urlpath), 200
    except IOError:
        return render_template('404.html'), 404


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
